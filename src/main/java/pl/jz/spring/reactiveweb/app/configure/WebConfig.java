package pl.jz.spring.reactiveweb.app.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.ViewResolverRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import pl.jz.spring.reactiveweb.app.domain.greeting.GreetingHandler;
import pl.jz.spring.reactiveweb.app.domain.pracownik.PracownikHandler;

import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Configuration(proxyBeanMethods = false)
@EnableWebFlux
public class WebConfig implements WebFluxConfigurer {

    @Bean
    public RouterFunction<ServerResponse> routeGreating(GreetingHandler greetingHandler) {
        return RouterFunctions
                .route(GET("/hello").and(accept(MediaType.APPLICATION_JSON)), greetingHandler::hello);
    }

    @Bean
    public RouterFunction<ServerResponse> routePracownik(PracownikHandler pracownikHandler) {
        return RouterFunctions
                .route(GET("/pracownik/{id}").and(accept(MediaType.APPLICATION_JSON)), pracownikHandler::findPracownikById)
                .andRoute(GET("/pracownik").and(accept(MediaType.APPLICATION_JSON)), pracownikHandler::getPracownikAll)
                .andRoute(POST("/pracownik").and(accept(MediaType.APPLICATION_JSON)), pracownikHandler::createPracownik)
                .andRoute(DELETE("/pracownik/{id}").and(accept(MediaType.APPLICATION_JSON)), pracownikHandler::deletePracownik);
    }

    // ...

    @Override
    public void configureHttpMessageCodecs(ServerCodecConfigurer configurer) {
        // configure message conversion...
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // configure CORS...
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        // configure view resolution for HTML rendering...
    }
}