package pl.jz.spring.reactiveweb.app.domain.pracownik;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class PracownikService {

    private final PracownikRepository pracownikRepository;

    Flux<Pracownik> getPracownicyAll(){
        return Flux.fromIterable(pracownikRepository.findAll());
    }

    Mono<Optional<Pracownik>> getPracownikById(Long id){
       return Mono.just(pracownikRepository.findById(id));
    }

    public Mono<Pracownik> savePracownik(PracownikDTO pracownikDTO) {
        Pracownik pracownik = Pracownik
                .builder()
                .id(pracownikDTO.getId())
                .firstName(pracownikDTO.getFirstName())
                .lastName(pracownikDTO.getLastName())
                .description(pracownikDTO.getDescription())
                .build();
        Pracownik newPracownik = pracownikRepository.save(pracownik);
        log.info(String.format("Add pracownik id: %d",newPracownik.getId()));
        return Mono.just(newPracownik);
    }

    public Long deletePracownik(Long id) {
        Pracownik pracownik = pracownikRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Pracownik o id= %d not found", id)));
        pracownikRepository.delete(pracownik);
        log.info(String.format("Delete pracownik id: %d",pracownik.getId()));
        return pracownik.getId();
    }
}
