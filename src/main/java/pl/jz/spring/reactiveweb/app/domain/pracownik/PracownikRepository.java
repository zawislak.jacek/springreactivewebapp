package pl.jz.spring.reactiveweb.app.domain.pracownik;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PracownikRepository extends CrudRepository<Pracownik,Long> {

}
