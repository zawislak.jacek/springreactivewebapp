package pl.jz.spring.reactiveweb.app.domain.pracownik;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Slf4j
@AllArgsConstructor
@Component
public class PracownikHandler {

    private final PracownikService pracownikService;

    public Mono<ServerResponse> getPracownikAll(ServerRequest request) {
        Flux<Pracownik> pracownik = pracownikService.getPracownicyAll();
        return ok().contentType(APPLICATION_JSON)
                .body(pracownik, Pracownik.class);
    }

    public Mono<ServerResponse> findPracownikById(ServerRequest request) {
        Long pracownikId = Long.parseLong(request.pathVariable("id"));
        Mono<ServerResponse> notFound = ServerResponse.notFound().build();
        return pracownikService.getPracownikById(pracownikId)
                .flatMap(pracownik -> ok().contentType(APPLICATION_JSON).bodyValue(pracownik)
                        .switchIfEmpty(notFound));
    }

    public Mono<ServerResponse> createPracownik(ServerRequest request) {
        return request.bodyToMono(PracownikDTO.class)
                .flatMap(pracownikService::savePracownik)
                .flatMap(newPracownik ->
                        ServerResponse.ok()
                                .contentType(MediaType.APPLICATION_JSON)
                                .bodyValue(newPracownik)
                );
    }


    public Mono<ServerResponse> deletePracownik(ServerRequest request) {
        return Mono.just(pracownikService.deletePracownik(Long.parseLong(request.pathVariable("id"))))
                .flatMap(val -> ServerResponse.noContent().build());
    }
}