package pl.jz.spring.reactiveweb.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import pl.jz.spring.reactiveweb.app.clients.GreetingClient;
import pl.jz.spring.reactiveweb.app.clients.PracownikClient;

@SpringBootApplication
public class SpringReactiveWebAppApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SpringReactiveWebAppApplication.class, args);
		GreetingClient greetingClient = context.getBean(GreetingClient.class);
		PracownikClient pracownikClient = context.getBean(PracownikClient.class);
		// We need to block for the content here or the JVM might exit before the message is logged
		System.out.println(">> message = " + greetingClient.getMessage().block());
		System.out.println(">> Pracownik = " + pracownikClient.getPracownik().collectList().block());
		System.out.println(">> Pracownik  o danym id = " + pracownikClient.getPracownikById(2L).block());
	}

}
