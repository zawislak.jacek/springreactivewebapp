package pl.jz.spring.reactiveweb.app.domain.manager;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@Component
@AllArgsConstructor
public class ManagerHandle {

    private final ManagerRepository managerRepository;

//    public Mono<ServerResponse> listManager(ServerRequest request) {
//        Flux<Manager> manager = Flux.fromIterable(managerRepository.findAll());
//        return ok().contentType(APPLICATION_JSON).body(manager, Manager.class);
//    }
//
//    public Mono<ServerResponse> createPerson(ServerRequest request) {
//        Mono<Person> person = request.bodyToMono(Person.class);
//        return ok().build(managerRepository.savePerson(person));
//    }
//
//    public Mono<ServerResponse> getPerson(ServerRequest request) {
//        int personId = Integer.valueOf(request.pathVariable("id"));
//        return managerRepository.getPerson(personId)
//                .flatMap(person -> ok().contentType(APPLICATION_JSON).bodyValue(person))
//                .switchIfEmpty(ServerResponse.notFound().build());
//    }
}