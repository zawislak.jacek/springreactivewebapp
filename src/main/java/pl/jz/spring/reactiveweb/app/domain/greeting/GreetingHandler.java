package pl.jz.spring.reactiveweb.app.domain.greeting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

/*
W podejściu Spring Reactive używamy handlera do obsługi żądania i tworzenia odpowiedzi
In the Spring Reactive approach, we use a handler to handle the request and create a response
 */
@Slf4j
@Component
public class GreetingHandler {

    public Mono<ServerResponse> hello(ServerRequest request) {
        Greeting greeting = new Greeting("Hello, Spring!");
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(greeting));
    }
}