package pl.jz.spring.reactiveweb.app.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.jz.spring.reactiveweb.app.domain.pracownik.Pracownik;
import pl.jz.spring.reactiveweb.app.domain.pracownik.PracownikRepository;

@Component
@RequiredArgsConstructor
public class DatabaseLoader implements CommandLineRunner {

    private final PracownikRepository pracownikRepository;


    @Override
    public void run(String... strings) throws Exception {
        this.pracownikRepository.save(new Pracownik("Frodo", "Baggins", "ring bearer"));
        this.pracownikRepository.save(new Pracownik("Jacek", "Zawislak", "Konopnica"));
        this.pracownikRepository.save(new Pracownik("Jacek1", "Zawislak1", "Konopnica"));
        this.pracownikRepository.save(new Pracownik("Jacek2", "Zawislak2", "Konopnica"));
    }
}