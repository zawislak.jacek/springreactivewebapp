package pl.jz.spring.reactiveweb.app.domain.manager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Arrays;
import java.util.Objects;

@NoArgsConstructor
//@RequiredArgsConstructor()
@Setter
@Getter
@Data
@Entity
public class Manager {

    //public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder(); (1)

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @JsonIgnore
    private String password;

    private String[] roles;

//    public void setPassword(String password) { (3)
//        this.password = PASSWORD_ENCODER.encode(password);
//    }

//    public Manager(String name, String password, String... roles) {
//
//        this.name = name;
//        this.setPassword(password);
//        this.roles = roles;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Manager manager = (Manager) o;
//        return Objects.equals(id, manager.id) &&
//                Objects.equals(name, manager.name) &&
//                Objects.equals(password, manager.password) &&
//                Arrays.equals(roles, manager.roles);
//    }
//
//    @Override
//    public int hashCode() {
//
//        int result = Objects.hash(id, name, password);
//        result = 31 * result + Arrays.hashCode(roles);
//        return result;
//    }
}