package pl.jz.spring.reactiveweb.app.clients;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import pl.jz.spring.reactiveweb.app.domain.pracownik.Pracownik;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class PracownikClient {

    private final WebClient client;

    public PracownikClient(WebClient.Builder builder) {
        this.client = builder.baseUrl("http://localhost:8181").build();
    }

    public Flux<Pracownik> getPracownik() {
        return this.client.get().uri("/pracownik").accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(Pracownik.class);
    }

    public Mono<Pracownik> getPracownikById(Long id) {
        return this.client.get().uri("/pracownik/{id}",id).accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Pracownik.class);
    }
}
