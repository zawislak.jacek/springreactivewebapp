package pl.jz.spring.reactiveweb.app.clients;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import pl.jz.spring.reactiveweb.app.domain.pracownik.Pracownik;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PracownikClientTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void routePracownik_get_when_pracownik_is_exist() {
        webTestClient
                .get()
                .uri("/pracownik/1")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Pracownik.class)
                .value(pracownik -> {
                    assertThat(pracownik.getId()).isEqualTo(1L);
                    assertThat(pracownik.getFirstName()).isEqualTo("Frodo");
                });
    }

    @Test
    void routePracownik_get_when_all_pracownicy() {
        webTestClient
                .get()
                .uri("/pracownik")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(Pracownik.class).hasSize(4);
    }
}