package pl.jz.spring.reactiveweb.app.clients;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import pl.jz.spring.reactiveweb.app.domain.greeting.Greeting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
//  We create a `@SpringBootTest`, starting an actual server on a `RANDOM_PORT`
// Tworzymy `@SpringBootTest`, uruchamiając rzeczywisty serwer na `RANDOM_PORT`.
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GreetingClientTest {

    // Spring Boot will create a `WebTestClient` for you,
    // already configure and ready to issue requests against "localhost:RANDOM_PORT"
    // Spring Boot utworzy dla Ciebie `WebTestClient`,
    // już skonfigurowanego i gotowego do wysyłania żądań na adres "localhost:RANDOM_PORT".
    @Autowired
    private WebTestClient webTestClient;

    @Test
    void testHello() {
        webTestClient
                // Create a GET request to test an endpoint
                // Utwórz żądanie GET, aby przetestować punkt końcowy
                .get().uri("/hello")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                // and use the dedicated DSL to test assertions against the response
                // i użyć dedykowanego języka DSL do testowania asercji względem odpowiedzi
                .expectStatus().isOk()
                .expectBody(Greeting.class).value(greeting -> {
            assertThat(greeting.getMessage()).isEqualTo("Hello, Spring!");
        });
    }
}